﻿using System;
using System.Collections;   // for Stack
using System.Collections.Generic;   // for Stack

namespace topic4_stack_ex
{
    class Program
    {
        static void Main(string[] args)
        {
            // You can use Stack to reverse a string.
            // Write a Console application that prompts a user to enter a string.
            // The program should use Stack to reverse and display the string.
            reverseStr();

            // 2.	You can use a Stack to check if a programming statement or a formula has balanced parentheses.
            // Write a Console application that prompts a user input to enter an expression with parenthesis.
            // The program should check the number of parentheses in the expression and display whether an expression is balance or not.
            checkStatment();
        }

        static void reverseStr()
        {
            Console.Clear();
            Console.Write("Type in any string to reverse and then press enter key : ");
            string str = Console.ReadLine();
            int cnt;

            Stack stack = new Stack();

            foreach(char c in str)
            {
                stack.Push(c);
            }
            
            cnt = stack.Count;
            for(var i=0; i<cnt;i++)
            {
                Console.Write(stack.Pop().ToString());
            }

            Console.WriteLine();
            Console.ReadLine();
        }

        static void checkStatment()
        {
            Console.Clear();
            Console.Write("Type in one statment of method : ");
            string str = Console.ReadLine();
            int cnt;
            byte oneByte = 0xff;


            Stack stack = new Stack();

            foreach(char c in str)
            {
                stack.Push(c);
            }

            cnt = stack.Count;
            for(var i=0; i<cnt;i++)
            {
                switch(stack.Pop())
                {
                    case '(' :
                        if((oneByte & 0b00001110) == 0)
                        {
                            oneByte &= 0b11111110;
                        }
                    break;
                    
                    case ')' :
                        if((oneByte & 0b00001100) == 0)
                        {
                            oneByte &= 0b11111101;
                        }
                    break;
                    
                    case '{' :
                        if((oneByte & 0b00001000) == 0)
                        {
                            oneByte &= 0b11111011;
                        }
                        
                    break;
                    
                    case '}' :
                        oneByte &= 0b11110111;
                    break;
                    
                    default: break;
                }
            }

            if(oneByte == 0b11110000)
            {
                Console.WriteLine("Banlanced method expression");
            }
            else
            {
                Console.WriteLine("Unbanlanced method expression");
            }

            Console.WriteLine();
            Console.ReadLine();

        }
    }
}
